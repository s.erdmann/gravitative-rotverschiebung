from __future__ import division
import numpy as np
from astropy.io import fits
from datetime import datetime
import matplotlib.pyplot as plt

# Path of the Picture: [date_time_type.tiff]
# TODO über Zweitliche Entwicklung ca. 10 min. Mitteln
# TODO NUmpy Integer Arrays verwenden
class Picture():
	# Class Attribute
	# Initializer / Instance Attributes
	
	def __init__(self, path):
		self.path = {'norm': path}
		self.imageData = self.loadImage()
		
		self.height, self.length = self.imageData.shape
		
	def show(self):
		#TODO auf Fits Files anpassen
		plt.imshow(self.imageData, cmap='gray')
		plt.colorbar()

	def loadImage(path):
		'''Lädt ein Fits Bild'''
		hdul = fits.open(path)
		#print(hdul.info())
		spektren = hdul[0].data
		#print(f"Image Shape:{spektren.shape}")
		return spektren

	def loadTimes(path):
		''' Lädt eine Datei mit Zeiten '''
		timesFile = open(path,'r')
		times = np.empty((1), dtype = np.float64)
		for line in timesFile:
			if line[0] is not '#':
				time = datetime.strptime(line, '%d,%m,%Y,%H,%M,%S,%f\n')
				tstamp = time.timestamp()
				np.append(times, tstamp)
		print(f"Times Shape: {times.shape}")
		return times
	#def getImg(self):
	#	'''Transforms a monochrome TIFF Picture into a 2D Array:
	#	Parameters:
	#	path (string): path of the Picture

	#	Returns:
	#	2D NmPy int Array: the Picture as monochrome 2D Array
	#	'''
	#	with Image.open(self.path['norm']).convert('L') as img:
	#		imageData = np.array(img)
	#	print(imageData[1][2])

	#	#TODO richtig drehen und evtl. Spiegeln
	#	#TODO Implementieren
	#	print(imageData.shape)
	#	return imageData

	#def imageData(self):
	#	''' Reads the Image Data (date, time, etc.) and returns it as a String
	#		Parameters:
	#		path (string): path of the Picture

	#		Returns:
	#		string: Image data
	#	'''
	#	with Image.open(self.path['norm']) as img:
	#		data = {TAGS[key]: img.tag[key] for key in img.tag.keys()}
	#	#TODO Implementieren
	#	return data

#	def darkfield(self, darkfieldPath):
#		''' Darkfield corrections
#			Parameters:
#				image(2D NmPy float Array) : Picture
#				darkfieldPath: Path to Darkfield Image
#
#			Returns
#			2D NumPy int Array: image
#		'''
#
#		self.path['dark'] = darkfieldPath
#		with Image.open(self.path['dark']).convert('L') as img:
#			field = np.array(img)
#		self.imageData -= field
#		return self.imageData
#

#	def flatfield(self, picture, flatfieldPath):
#		''' flatfield corrections
#			Parameters:
#				image(2D NmPy float Array) : Picture
#				flatfield : flatfield Image
#
#			Returns
#			2D NumPy int Array: corrected image
#		'''
#		self.path['flat'] = flatfieldPath
#
#		with Image.open(self.path['flat']).convert('L') as img:
#			field = np.array(img)
#		avgFlat = np.average(field)
#		self.imageData = self.imageData * field * avgFlat
#		return self.imageData
#
#	def to1DSpectrum(self, isSpectrum=False):
#		'''
#		Parameter:
#			image(2D NmPy int Array) : Picture[lines][rows]
#			isSpectrum (boolean): True if the image does only consist of a spectrum. Fals if not.
#		Returns:
#			[int]
#		'''
#		avgLine = 0
#		for line in self.imageData:
#			avgLine += (np.average(line))
#		avgLine/= self.height
#

