
#TODO Lade eine Serie von Bildern und mache bei diesen eine Darkfieldkorrektur.
#TODO Später auch Flatfieldkorrektur machen
from __future__ import division


import numpy as np
from astropy.io import fits
import os
import sys
from datetime import datetime
import matplotlib.pyplot as plt

def getSortedNames(path, timeString, fileType='fits'):
	flist = [fname for fname in os.listdir(path=path) 
	if fname.startswith(timeString)
		and fname.endswith(fileType)
		and not 'complete' in fname
		and not 'av' in fname
		and not 'gesamt' in fname]
	
	sort = lambda fname: fname[-11:-5]

	return sorted(flist, key=sort)

def averageDarks(darkTime, darkPath, save=True):
	'''darkTime: ein eindeutiger schlüssel für der in alle Dateinamen vorkommt
	darkPath: der dateipfad
	save: Wenn True, dann wird das ergebis in darktime_avDark.fits gespeichert. '''
	darkNames = getSortedNames(darkPath, darkTime)

	hdul = fits.open(darkPath+darkNames[0])
	shape = hdul[0].data.shape
	dtype = np.float64
	darkArray = np.zeros(shape, dtype=dtype)
	for fname in darkNames:
		hdul = fits.open(darkPath + fname)
		darkArray += hdul[0].data
		hdul.close()
	
	darkArray /= len(darkNames)
	savePath = darkPath + darkTime + '_avDark.fits'
	if save:
		hdr = fits.Header()
		hdr['OBSERVER'] = 'Ruben'
		hdr['COMMENT'] = 'Dark'
		hdu = fits.PrimaryHDU(header = hdr)
		hdu.data = darkArray
		hdu.writeto(darkPath + darkTime + '_avDark.fits')
	return darkArray

def averageFlats(flatTime, flatPath, save=True):
	flatNames = getSortedNames(flatPath, flatTime)

	hdul = fits.open(flatPath+flatNames[0])
	shape = hdul[0].data.shape
	dtype = np.float64
	flatArray = np.zeros(shape, dtype=dtype)
	for fname in flatNames:
		hdul = fits.open(flatPath + fname)
		flatArray += hdul[0].data
		hdul.close()
	
	flatArray /= len(flatNames)
	savePath =flatPath + flatTime + '_avflat.fits'
	if save and not os.path.exists(savePath):
		hdr = fits.Header()
		hdr['OBSERVER'] = 'Ruben'
		hdr['COMMENT'] = 'flat'
		hdu = fits.PrimaryHDU(header = hdr)
		hdu.data = flatArray
		hdu.writeto(savePath)
	else:
		print('Das hast du schonmal gemacht :) Es wird jetzt nicht neu gespeichert')
	return flatArray

#def getFlatErrors(flatTime, flatPath, darkArray, save=True):
#	flatNames = getSortedNames(flatPath, flatTime)
#
#	hdul = fits.open(flatPath+flatNames[0])
#	shape = hdul[0].data.shape
#	dtype = np.float64
#	flatArray = np.zeros(shape, dtype=dtype)
#	for fname in flatNames:
#		hdul = fits.open(flatPath + fname)
#		flatArray += hdul[0].data
#		hdul.close()
#	flatArray /= len(flatNames)
#	flatArray -= darkArray
#
#	#Berechne Flatfield Spektrum und normalisiere das
#	counts = np.sum(flatArray, axis=0, dtype=dtype)
#	counts /= max(counts)
#
#	fehlerFormel = lambda X: np.sqrt(2* X)
#	errors = fehlerFormel(counts)
#
#	savePath =flatPath + flatTime + '_avErrors.fits'
#	if save and not os.path.exists(savePath):
#		hdr = fits.Header()
#		hdr['OBSERVER'] = 'Ruben'
#		hdr['COMMENT'] = 'error'
#		hdu = fits.PrimaryHDU(header = hdr)
#		hdu.data = flatArray
#		hdu.writeto(savePath)
#	else:
#		print('Das hast du schonmal gemacht :) Es wird jetzt nicht neu gespeichert')
#	return errors


def applyCorrections(imageTime, imagePath, darkArray=None, flatArray=None, save=True):
	#TODO nicht nur Darks, sondern auch Flats direkt anfügen
	#TODO Fehler berechnen
		

	imageNames = getSortedNames(imagePath, imageTime)
	spektren = np.empty((3664, 0), dtype= np.int32)
	fehler = np.zeros((3664,1), dtype= np.float64)
	for fname in imageNames:
		try:
			hdul = fits.open(imagePath+fname)
			data = hdul[0].data
		except:
			print(fname)
			print(sys.exc_info())
		if darkArray is not None:
			data = data - darkArray
		#TODO Fehler berechnen
		
		if flatArray is not None:
			data = data * np.average(flatArray.flatten())  / flatArray
		data = data.reshape(data.shape[0], data.shape[1], 1)
		#err = fehler.reshape(fehler.shape[0], fehler.shape[1])
		av = np.sum(data, axis=0, dtype=np.int32)
		avErr = np.sqrt(2*np.sum(data, axis=0, dtype=np.int32))
		hdul.close()
		spektren = np.append(spektren, av, axis=1)
		fehler += avErr

	fehler /= len(imageNames)


	savePath =imagePath + imageTime + '_avGesamt.fits'
	if save and not os.path.exists(savePath):
		hdr = fits.Header()
		hdr['OBSERVER'] = 'Ruben'
		hdr['COMMENT'] = 'Ergebnis'
		hdu = fits.PrimaryHDU(header = hdr)
		hdu.data = spektren
		hdu.writeto(savePath)
	else:
		print('Die Datei gibt es schon Speichere nicht')

	errorPath = imagePath + imageTime + '_avErr.fits'
	if save and not os.path.exists(errorPath):
		hdr = fits.Header()
		hdr['OBSERVER'] = 'Ruben'
		hdr['COMMENT'] = 'Ergebnis'
		hdu = fits.PrimaryHDU(header = hdr)
		hdu.data = fehler
		hdu.writeto(errorPath)
	else:
		print('Die Datei gibt es schon Speichere nicht')
	return spektren, fehler



########### Wenn ich das Spektrum erstmal habe geht der Rest hier mit

def show(imageArray):
	''' Zeigt das Bild An'''
	plt.imshow(imageArray, cmap='gray')
	plt.colorbar()

def loadImage(path):
	'''Lädt ein Fits Bild und gibt es als Numpy Array zurück'''
	hdul = fits.open(path)
	#print(hdul.info())
	spektren = hdul[0].data
	#print(f"Image Shape:{spektren.shape}")
	return spektren

def loadTimes(path):
	''' Lädt eine Datei mit Zeiten und gibt diese als Numpy Array zurück'''
	timesFile = open(path,'r')
	times = np.empty((0), dtype = np.float64)
	for line in timesFile:
		if line[0] is not '#':
			time = datetime.strptime(line, '%d,%m,%Y,%H,%M,%S,%f\n')
			tstamp = time.timestamp()
			times = np.append(times, [tstamp])
	return times