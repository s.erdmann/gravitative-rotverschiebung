from Printing import Drift_Intendant, getColors
from Atlas import Spec_Atlas 


class Evaluator():

    def __init__(self, lines, keys, times):
        self.lines = lines
        self.keys = keys
        self.times = times
    
    def evaluate(self, show=True, absolute=(False, False), compare=(False, False), mean=(False, False)):

        if show:    
            colors = getColors([len(t) for t in self.keys])
            intendant = Drift_Intendant()

        Coords, Errors = [], []
        for t in range(len(self.keys)):
            Coords.append([]), Errors.append([])
            for l in range(len(self.keys[t])):
                Coords[-1].append([lines[l][0] for lines in self.lines[t]])
                Errors[-1].append([lines[l][1] for lines in self.lines[t]])

                # TODO: Mean calculation
                mean_val = [Coords[-1][-1][0], Errors[-1][-1][0]]  # TODO
                if not absolute[t]: mean_val[0] -= Coords[-1][-1][0]
                compare_val = list(Spec_Atlas[self.keys[t][l]])
                if not absolute[t]: compare_val[0] -= Coords[-1][-1][0]

                if not absolute[t]: Coords[-1][-1] = [c-Coords[-1][-1][0] for c in Coords[-1][-1]]

                if show:    intendant.addDrift(Coords[-1][-1], Errors[-1][-1], type=t, color=colors[t][l], label=self.keys[t][l], compare=compare_val, mean=mean_val)

        if type(self.times) in (int, float): times = [i*self.times for i in range(len(Coords[-1][-1]))]
        else: times = self.times
        time_errs = [0 for i in times]

        if show: intendant.plot(times, time_errs, absolute, compare, mean)

        return Coords, Errors