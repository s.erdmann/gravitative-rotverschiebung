import numpy as np
from scipy.optimize import curve_fit
from scipy.signal import argrelmin
from matplotlib import pyplot as plt
from Atlas import Spec_Atlas

#TODO Verständliche Legende
#TODO Verständliche Aufgabe
def doFit(data, ref_keys, sun_keys):
    """
    Calculate wavelengths of sun lines through fitting of all lines, identifying reference lines and calculating wavelength solution and with that sun wavelengths.
    Parameters:
    -----
    data (1d np.array): detected intensitiy curve\n\n
    ref_keys (list(str)): keys of reference lines in here\n\n
    sun_keys : same for sun lines
    Returns:
    -----
    (list(tupel)): Sun wavelengths with errors
    """

    all_pxls = findAllLines(data, len([*ref_keys, *sun_keys]))                                      #   Get pixel coordinates of all lines to find here.

    ref_pxls, ref_wavs, sun_pxls, sun_wavs = sortLines(all_pxls, ref_keys, sun_keys)                #   Identify and get measure and expected of both reference and sun lines.

    PtoWP, PtoWPe = findWavelSolution(ref_pxls, ref_wavs, len(data))                                #   Find Wavelength solution for detector calibration

    sun_wavs_fit = calculateWavels(sun_pxls, PtoWP, PtoWPe)                                         #   Use that for computing sun wavelengths

    return sun_wavs_fit, sun_wavs


def findAllLines(data, number, order=10):
    """
    Calculate wavelengths of sun lines through fitting of all lines, identifying reference lines and calculating wavelength solution and with that sun wavelengths.
    \n ### ### Change order for effectively altering results. ### ###
    Parameters:
    -----
    data (1d np.array): detected intensitiy curve\n\n
    number (int): number of lines to find\n\n
    order (int): minimal distances of lines to find
    Returns:
    -----
    (list(tupel)): found lines coordinates with errors
    """

    xdata = np.array(range(data.size))                                                              #   Xdata as base of fit out of number of data points

    xmax, ymax = max(xdata), max(data)                                                              #   Borders of provided data are used for calculation of parameter bounds in fit.
    initial, bounds = [ymax], ([ymax*0.5], [ymax*2])                                                #   Initalisation of initial guesses and bounds: firstly constant offset

    mins = list(argrelmin(data, order=order)[0])                                                    #   Initial list of minima in data to sort out from
    min_vals = [data[min] for min in mins]                                                          #   Their values

    for i in range(number):                                                                         #   For as much lines as to find do:
        m = np.argmin(min_vals)                                                                     #   Index of minimalest minimum
        min = mins[m]                                                                               #   Its location
        del mins[m], min_vals[m]                                                                    #   Delete them from their lists

        initial.extend([0.5*ymax, min, order])                                                      #   Initial values for Gauss-curve added: Amplitude as half of max intensity, location as location of that minimum, stdev as order
        bounds[0].extend([0, 0, 0])                                                                 #   Bounds for Gauss-curves: all positive
        bounds[1].extend([ymax, xmax, xmax/2])                                                      #   Upper bounds: Amplitude as max intensity, location as detector width, stdev as half detector width

    ###### Initial values and bounds are necessary to provide the results searched for ######
    params, errors = curve_fit( gaussPol, xdata, data, initial, bounds=bounds )                     #   Returs optimal parameters of gaussians and covariance matrix or stdevs for them.
    errors = np.sqrt(np.diag(errors))                                                               #   Standart deviations are calculatet from covariance matrix.

    show(xdata = xdata, ydata = data, newxdata = xdata, newydata = gaussPol(xdata, *params), 
         title = "Detected spectrum", xlabel = "Detector coordinate [pixel]", ylabel = "Intensity")

    lines = [(params[i*3+2], errors[i*3+2]) for i in range(int((len(params)-1)/3))]                 #   List of line tupels are formed: location and its error
    lines = [tpl for tpl in sorted(lines, key=lambda x:x[1])]                                       #   Sort them by height

    return lines


def sortLines(all_pxls, ref_keys, sun_keys):
    """
    Identify and sort reference and sun lines through height
    Parameters:
    -----
    all_pxls (list(float)): pixel coordinates of all lines\n\n
    ref_keys (list(str)): Atlas keys of reference lines in here\n\n
    sun_keys (list(str)): Atlas keys of sun lines in here
    Returns:
    -----
    ref_pxls (list(tupel(floats))): pixel coordiates of reference lines with error\n\n
    ref_wavs (list(float)): expected wavelengths of reference lines\n\n
    sun_pxls (list(tupel(floats))): pixel coordiates of sun lines with error\n\n
    sun_wavs (list(float)): expected wavelengths of sun lines
    """

    all_wavs = {k: Spec_Atlas[k] for k in [*ref_keys,*sun_keys]}                                    #   Dict for all lines of use here, extracted from Global Atlas
    all_wav_keys = [tpl[0] for tpl in sorted(all_wavs.items(), key=lambda x:x[1])]                  #   All line keys sorted by their coresponding wavelength

    ref_locs = [i for i in range(len(all_wavs)) if all_wav_keys[i] in ref_keys]                     #   Positions of reference lines in list of all lines from left to right
    ref_pxls = [all_pxls[i] for i in ref_locs]                                                      #   Their pixel coordinates in provided data   (unsorted because of?)
    ref_pxls = [tpl for tpl in sorted(ref_pxls, key=lambda x:x[0])]                                 #   Sorted by height

    sun_locs = [i for i in range(len(all_wavs)) if all_wav_keys[i] in sun_keys]                     #   Positions of sun lines in list of all lines from left to right
    sun_pxls = [all_pxls[i] for i in sun_locs]                                                      #   Their pixel coordinates in provided data   (unsorted because of?)
    sun_pxls = [tpl for tpl in sorted(sun_pxls, key=lambda x:x[0])]                                 #   Sorted by height

    ref_wavs = [all_wavs[k] for k in ref_keys]                                                      #   Wavelength of ref lines
    ref_wavs.sort()                                                                                 #   Sorted by height

    sun_wavs = [all_wavs[k] for k in sun_keys]                                                      #   Wavelength of sun lines
    sun_wavs.sort()                                                                                 #   Sorted by height

    return ref_pxls, ref_wavs, sun_pxls, sun_wavs


def findWavelSolution(pxls, wavs, width):
    """
    Provide parameter set for polynomial wavelength calibration
    Parameters:
    -----
    pxls (list(tupel(floats))): pixel coordinates of calibration lines with error\n\n
    wavs (list(float)): their expected wavelengths from Atlas\n\n
    width (int): image width
    Returns:
    -----
    params (list(float)) \n\n
    errors (list(float))
    """

    pxls_errs = np.array([p[1]] for p in pxls)                                                      #   Errors in pixel coordinates
    pxls = np.array([p[0] for p in pxls])                                                           #   Values
    
    bounds = ([0 for i in range(len(wavs))], [1e6 for i in range(len(wavs))])                       #   All polyomial params to be positive and not indefinit
    params, errors = curve_fit(polynom, xdata=pxls, ydata=wavs, p0=[1 for i in range(len(wavs))], bounds=bounds)    #   Returs optimal parameters of gaussians and covariance matrix or stdevs for them.
    errors = np.sqrt(np.diag(errors))                                                               #   Standart deviations are calculatet from covariance matrix.

    show(xdata = pxls, ydata = wavs, newxdata = np.arange(0,width), newydata = polynom(np.arange(0,width), *params),
         xlabel ="Detector coordinate [pixel]", ylabel = "Wavelength [nm]", title = "Wavelength solution")

    return params, errors


def calculateWavels(pxls, params, errors):
    """
    Calculate wavelengths out of pixels with polynomial wavelength calibration.
    Parameters:
    -----
    pxls (list(tupel(floats))): pixel coordinates of lines with error\n\n
    params (list(float)): parameters for polynomial wavelength solution\n\n
    errors (list(float)): standart deviations in them
    Returns:
    -----
    wavels  (list(tupel(floats))):  wavelengths with error
    """

    wavels = []                                                                             	    #   Initalize list
    for pxl in pxls:                                                                                #   For every pixel coordinate tupel do:
        wav = polynom(pxl[0], *params)                                                              #   Compute wavelength out of polynomial wavelength solution
        err = polynomErr(pxl[0], pxl[1], params, errors)                                            #   And their errors
        wavels.append((wav, err))                                                                   #   Add to list of tupels

    #wavels = [ (polynom(pxl[0], *params), polynomErr(pxl[0], pxl[1], params, errors)) for pxl in pxls ]    #   Alternative one-liner

    return wavels



def polynom(x, *a):
    f = 0
    for i in range(len(a)): f += x**i * a[i]
    return f

def polynomErr(x, dx, a, da):
    #TODO:  Maybe compute polniomial gauss-error with sympy
    f = 0
    for i in range(len(a)): f += x**i * a[i]
    return f

def gauss(x, A, l, s):  return  A * np.exp(-0.5 * ((x-l)/s)**2)

def gaussPol(x, c, *ps):
    f = c
    for i in range(int(len(ps)/3)): f -= gauss(x, ps[3*i], ps[3*i+1], ps[3*i+2])
    return f



def show(xdata, ydata, newxdata, newydata, title, xlabel, ylabel):
    plt.close()
    plt.figure(figsize=(15,5))
    plt.plot(xdata, ydata)
    plt.plot(newxdata, newydata)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.show()




###### TEST ######
