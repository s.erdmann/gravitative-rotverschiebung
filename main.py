##########    Gathering and Polishing    ##########
def ruben():
    from Corrections import Picture

    path = 'C:\\CODE\\Foll 2020\\Reduction\\data\\lars_diskcenter.tiff'
    pic = Picture(path)
    #pic.show()
    #pic.cosmetics(darkfieldPath='', flatfieldPath='./lars_flat.tiff')
    spectrum = pic.to1DSpectrum(isSpectrum=True)
    return spectrum

### provisorisches Erweitern der daten von Rubens pipeline
def pleaseDelete(spectrum):
    import numpy as np
    import numpy.random.mtrand as rd
    datas, errs, times = [spectrum.copy() for i in range(10)],    [10 for i in range(len(spectrum))],    0.5
    for i in range(1, len(datas)):
        Max = max(datas[i])
        for j in range(len(datas[i])):  datas[i][j-int(i**1.4)] = datas[i][j] + Max*rd.randint(10)/1000 #* np.sin((j+np.pi*i/20/4)/len(datas[i])*124) // 25
    return datas, errs, times




### Provisorisches Substitut für Rubens pipeline. Generation of test data. Delete when Gathering and Polishing ready
def getTestData(file):
    import test
    data = test.read(file, sum=True)
    datas = [data]
    errs = [10 for i in range(len(data))]
    return datas, errs, 0.5

##########    Upsetting, Fitting and Evaluating    ##########
def beni(datas, errs, times):
    import Upsetting, Fitting, Evaluating

    show, absolute, compare, mean = [True, [0,-1], True], (False, False), (False, False), (True, True)

    refs, suns, slice = Upsetting.Upsetter().setup(show=show[0], datas=datas, errs=errs)
    athmos, solars = Fitting.Fitter(ref_keys=refs, sun_keys=suns, peak_order=3, voigt_width=[0.8, 0.8],  gauss_width=0.003/2*8).fit(show=show[1], datas=datas, errs=errs, slice=slice, compare=compare) 
    #coords, errors = Evaluating.Evaluator(lines=[athmos,solars], keys=[refs,suns], times=times).evaluate(show=show[2], absolute=absolute, compare=compare, mean=mean)


#beni(*getTestData(file='data/lars-spc_03dec2013-153650_diskcenter_fake.fits'))
#beni(*pleaseDelete(ruben()))