'''
Global dictionary containig spectral lines. To be used as reference for wavelength solution or shift calculation. Floats in nanometers.
'''
#TODO Wellenlängen genauer herausfinden!!!
Spec_Atlas = {'a':      630.167, 
              'atm_a':      630.2,
              'b':      630.249,
              'atm_b':      630.275}
