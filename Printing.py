import matplotlib.pyplot as plt
from datetime import datetime as date
import numpy as np

def getColors(lengths):
    return [[[['#00%xff','#ff%x00'][i]  % (128+j*(64//(linls[i]-1))) for j in range(linls[i])] for i in range(len(linls))] for linls in [lengths]][0]   

class Intendant():

    # Fancyness 0-2
    def __init__(self, fancy=0): self.fancy = fancy
        
    def plot(self, data=None, errs=None, xdata=None, xerrs=None, zero=0, title='', xlabel='', ylabel='', label='', fmt='--', c=None, show=True):
        if self.fancy: plt.style.use("dark_background")
        if not c: c = '#ffffff88' if self.fancy else '#00000088'
        
        self.fig, self.axe = plt.subplots(figsize=(15,5))

        if data is not None:
            if errs is None: errs = [0 for i in data]
            if xdata is None: xdata = range(zero, len(data)+zero)
            if xerrs is None: xerrs = [0 for i in data]
    
            errorbarplot = self.axe.errorbar(xdata, data, xerr=xerrs, yerr=errs, fmt=fmt, capsize=2, elinewidth=0.5, label=label, color=c, zorder = 0)
            errorbarplot.lines[0].is_glow_line = True
            for cap in errorbarplot.lines[1]: cap.is_glow_line = True

        self.axe.set_title(title)
        self.axe.set_xlabel(xlabel)
        self.axe.set_ylabel(ylabel)

        plt.minorticks_on()

        if self.fancy:
            self.axe.set_facecolor('#111111bb')
            self.fig.set_facecolor('#050505')
            self.fig.set_edgecolor('#8800ff')

        self.axe.tick_params(which="major", direction="out", length=6,width=2)
        self.axe.grid(b=True,which="major",linestyle="-",alpha=0.4)
        self.axe.grid(b=True,which="minor",linestyle="--",alpha=0.25)

        if self.fancy: self.axe.tick_params(colors='white', which='both')
        if show:    self.show()

    def show(self):
        self.axe.legend(fancybox=False, framealpha=0.25)
        if self.fancy > 1: self.addGlow()
        #plt.savefig('output\\'+str(date.now()).replace(':','_')+'.pdf', bbox_inches='tight')
        plt.show()

    def addGlow(self, ax=None, n_glow_lines=10, diff_linewidth=1.05, alpha_line=0.3):
        """From mplcyberpunk"""
        ax = self.axe
        if not ax:  ax = plt.gca()
        lines = ax.get_lines()
        alpha_value = alpha_line / n_glow_lines
        for line in lines:
            data = line.get_data()
            linewidth = line.get_linewidth()
            for n in range(1, n_glow_lines + 1):
                glow_line, = ax.plot(*data)
                glow_line.update_from(line)  # line properties are copied as seen in this solution: https://stackoverflow.com/a/54688412/3240855
                glow_line.set_alpha(alpha_value)
                glow_line.set_linewidth(linewidth + (diff_linewidth * n))
                glow_line.is_glow_line = True  # mark the glow lines, to disregard them in the underglow function.


class Spectral_Intendant(Intendant):

    def __init__(self):
        super().__init__()
        self.lines = []

    def addProfile(self, model, params, slice=slice(0,-1), color="lime", label='line', compare=None):
        self.lines.append(ProfilePlot(model, params, slice, color, label, compare))

    def plot(self, data, errs, calib, zero, index, compare): 
        xdata = calib.calc(range(zero, zero + len(data)), list=True)

        super().plot(data=data, errs=errs, xdata=xdata, zero=zero, title='Spectral Fit', label='Corrected input,\nimage row '+str(index+1), xlabel='Wavelength[nm]', ylabel='Intensity []', show=False)

        for line in self.lines: 
            
            
            xdatb = range(line.slice.start, line.slice.stop)
            linb = np.linspace(line.slice.start, line.slice.stop-1, 100)
            slic = xdata[line.slice]
            lin = np.linspace(slic[0], slic[-1], 100)

            self.axe.plot(lin, line.calc(linb), color=line.color, label=line.label)
            self.axe.axvline(x=calib.calc(line.params[0]+zero), color=line.color)
            self.axe.axvspan(slic[0], slic[-1], color=line.color+'22')
            #if compare and line.compare:   
            #    self.axe.axvline(x=line.compare[0], color=line.color+'aa', linestyle='--')
            #    self.axe.axvspan(line.compare[0]-line.compare[1], line.compare[0]+line.compare[1], color=lizne.color+'11')

        self.axe.axvspan(xmin=calib.calc(zero), xmax=calib.calc(zero),color='#ffffff88', label='Fitting Interval') #,ymax=0
        #if compare: self.axe.axvline(x=calib.calc(zero),ymax=0,color='#ffffff88', label='Expected Position', linestyle='--')

        self.show()


class ProfilePlot():
    def __init__(self, model, params, slice, color, label, compare): self.model, self.params, self.slice, self.color, self.label, self.compare = model, params, slice, color, label, compare
    def calc(self, x): return self.model(x, *self.params)

class DriftPlot():
    def __init__(self, coords, coord_errs, color, label, compare, mean): self.coords, self.coord_errs, self.color, self.label, self.compare, self.mean = coords, coord_errs, color, label, compare, mean


class Drift_Intendant(Intendant):
    
    def __init__(self):
        super().__init__()
        self.solars, self.athmos = [], []
    
    def addDrift(self, coords, coord_errs, type, color='#8800ff', label='Line Drift', compare=None, mean=None): 
        if not coord_errs:  coord_errs = [0 for i in coords]
        
        drift = DriftPlot(coords, coord_errs, color, label, compare, mean)
        if type == 0:   self.athmos.append(drift)
        elif type == 1: self.solars.append(drift)

    def plot(self, times, time_errs, absolute, compare, mean):

        super().plot(title='Athmospheric Line Drift', ylabel=('' if absolute[0] else 'Relative ')+'Detector Coordinate [pxl]', xlabel = 'Observation time [s]', show=False) 
        for drift in self.athmos: 
            self.axe.errorbar(times, drift.coords, yerr=drift.coord_errs, xerr=time_errs, fmt='--', capsize=2, elinewidth=0.5, label='Athmospheric  '+drift.label, color=drift.color)            
            if compare[0] and drift.compare:
                y = drift.compare[0]-(drift.coords[0] if not absolute[0] else 0)
                self.axe.axhline(y=y, color=drift.color+'88', linestyle=':', label='Expected Position')
                self.axe.axhspan(y-drift.compare[1], y+drift.compare[1], color=drift.color+'22')
            if mean[0] and drift.mean:
                y = drift.mean[0]-(drift.coords[0] if not absolute[0] else 0)
                self.axe.axhline(y=y, color=drift.color+'aa', linestyle='-', label='Mean Position')
                self.axe.axhspan(y-drift.mean[1], y+drift.mean[1], color=drift.color+'22')
        self.show()

        super().plot(title='Solar Line Shift Evolution', ylabel=('' if absolute[1] else 'Relative ')+'Wavelength [nm]', xlabel = 'Observation time [s]', show=False)         
        for drift in self.solars:   
            self.axe.errorbar(times, drift.coords, yerr=drift.coord_errs, xerr=time_errs, fmt='--', capsize=2, elinewidth=0.5, label='Solar  '+drift.label, color=drift.color)            
            if compare[1] and drift.compare:
                y = drift.compare[0]-(drift.coords[0] if not absolute[1] else 0)
                self.axe.axhline(y=y, color=drift.color+'88', linestyle=':', label='Expected Position')
                self.axe.axhspan(y-drift.compare[1], y+drift.compare[1], color=drift.color+'22')
            if mean[1] and drift.mean:
                y = drift.mean[0]-(drift.coords[0] if not absolute[1] else 0)
                self.axe.axhline(y=y, color=drift.color+'aa', linestyle='-', label='Mean Position')
                self.axe.axhspan(y-drift.mean[1], y+drift.mean[1], color=drift.color+'22')

        self.show()