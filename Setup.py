import matplotlib.pyplot as plt
from matplotlib.widgets import TextBox
import pylab

#TODO Verständliche Legende für die Programme
class Setup():
    '''
    Interface to select data-range, number spectral lines to be recognized on and index sepctral lines for referrence (wavelength solution).
    '''

    #def __init__(self):                                                                      #   Indizees of spectral lines for absolute calibration in these
     #   self.borders = [0,0]                                                                    #   Borders for slicing data

    def setBorders(self, val, lr):
        ''' Set and update margins of data for later evaluation. Called by slider.on_changed. '''
        self.borders[lr] = val
        self.ax.set_xlim(self.borders)


    def submit(self):
        ''' Retrieve and save values from and finish plot window. Called by Button on click. '''

        for i in range(len(self.borders)):  self.borders[i] = int(self.borders[i])

        self.refs = [ref.strip() for ref in self.box_refs.text.split(',')]
        self.suns = [sun.strip() for sun in self.box_suns.text.split(',')]
		
        plt.close()


    def setup(self, data):
        '''
        Starts setup window for setting values on visualized data.
        Parameter:
        ------
        data (np.array(int)): 1d array containing integer values (cleaned spectrum)
        '''       
        
        plt.close()
        fig, self.ax = plt.subplots(figsize=(15,5))
        plt.subplots_adjust(left=0.1, bottom=0.15)

        self.plot, = plt.plot(data,"x--")  # Datenpunkte mit Fehlerbalken
        self.ax.set_xlabel('Detector coordinate [pixel]')            # Achsen-Label sind Spalten-Namen
        self.ax.set_ylabel('Intensity [int]')
        self.ax.set_title('Corrected spectrum')

        self.borders = [0, len(data)]

        slider_left = pylab.Slider(plt.axes([0.1,0.005,0.325,0.02]),'Left cut [pixel]', self.borders[0], self.borders[1], valinit=self.borders[0],valstep=1)
        slider_right = pylab.Slider(plt.axes([0.55,0.005,0.325,0.02]),'Right cut [pixel]', self.borders[0], self.borders[1], valinit=self.borders[1],valstep=1)

        setBorders = [lambda val, lr = i: self.setBorders(val, lr) for i in [0,1]]

        slider_left.on_changed(setBorders[0])
        slider_right.on_changed(setBorders[1])

        self.box_refs = TextBox(plt.axes([0.31,0.05,0.1,0.03]), 'List of line keys for spectral calibration (separeted by \',\') is ', initial='')
        self.box_suns = TextBox(plt.axes([0.8,0.05,0.1,0.03]), 'List of line keys for shift calculation is ', initial='')


        button_submit = ''  # Button dessen Aktion es ist, die Werte einzulesen, den Plot zu beenden und die Werte zurückzugeben

        plt.show()

        self.submit()
        return self.borders, self.refs, self.suns



#Setup().setup(data=[1230,3331,2312,123123,435,123,23,12,23,12,12331,4,123124,1241])

# make window
# plot data
# return #lines, [O2s]
